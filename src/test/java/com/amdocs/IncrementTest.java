package com.amdocs;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class IncrementTest {



    @Test
    public void testDecreseCounterInputZero()  {
	int result  = new Increment().decreasecounter(0);
	assertEquals(1, result);
    }

	
   @Test
    public void testDecreseCounterInputOne()  {
        int result  = new Increment().decreasecounter(1);
        assertEquals(1, result);
    }

   @Test
    public void testDecreseCounterInputMoreThanOne()  {
        int result  = new Increment().decreasecounter(3);
        assertEquals(1, result);
    }

    @Test
    public void testGetCounter()  {
        int result  = new Increment().getCounter();
        assertEquals(1, result);
    }
}
